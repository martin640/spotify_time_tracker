export interface Config {
    app: {
        bind: string;
        port: number;
    };
    mysql: {
        host: string;
        user: string;
        password: string;
        database: string;
    };
    spotify: {
        clientId: string;
        clientSecret: string;
        refresh?: number;
        accessToken?: string;
        refreshToken?: string;
        expires?: number;
    };
    filler?: {
        [key: string]: [number, number];
    };
    _appCache?: {
        profile?: UserProfile;
    }
}
export interface HistoryEntryInsert {
    id: string;
    timestamp: number;
    title: string;
    artist: string;
    duration: number;
    image: string;
}
export interface HistoryEntrySchema extends HistoryEntryInsert {
    timestamp_sql: Date;
    month: string;
    week: string;
}

// spotify response objects
export interface ImageRef {
    width: number;
    height: number;
    url: string;
}
export interface BaseUniqueObject {
    external_urls: object;
    href: string;
    id: string;
    name: string;
    type: string;
    uri: string;
}
export interface AlbumPartial extends BaseUniqueObject {
    album_type: string;
    artists: object[];
    available_markets: string[];
    images: ImageRef[];
    release_date: string;
    release_date_precision: string;
    total_tracks: number;
}
export interface ArtistPartial extends BaseUniqueObject { }
export interface TrackPartial extends BaseUniqueObject {
    album: AlbumPartial;
    artists: ArtistPartial[];
    available_markets: string[];
    disc_number: number;
    duration_ms: number;
    explicit: boolean;
    external_ids: object;
    is_local: boolean;
    popularity: number;
    preview_url: string|null;
    track_number: number;
}
export interface UserProfile extends BaseUniqueObject {
    display_name: string;
    followers: {
        href: string|null;
        total: number;
    }
    images: ImageRef[];
}
export interface RecentlyPlayedItem {
    track: TrackPartial;
    played_at: string;
    context: object;
}
export interface RecentlyPlayedResponse {
    items: RecentlyPlayedItem[];
    next: string|null;
    cursors: any;
    limit: number;
    href: string|null;
}

// database queries
export interface DbHistoryEntry {
    image: string;
    title: string;
    artist: string;
    id: string;
    duration: number;
}
export interface DbHistoryRangeEntry extends DbHistoryEntry { timestamp: number; }
export interface DbHistoryCounterEntry extends DbHistoryEntry { count: number; }
export interface DbSimpleStat<T> { x: T; y: number; }
export interface DbSeasonStat {
    duration: number;
    songs: number;
}
export interface SingletonResponse {
    last100: DbHistoryRangeEntry[];
    top10: DbHistoryCounterEntry[];
    currentSeason: DbSeasonStat;
    previousSeason: DbSeasonStat;
    lastEntry: number;
    nextUpdate: number;
    username: string;
    weekly: DbSimpleStat<string>[];
    monthly: DbSimpleStat<string>[];
}

import * as path from 'path'
import { default as Fastify } from 'fastify'
import fastifyStatic from '@fastify/static'
import * as db from './db'
import routes from './routes'

process.on('uncaughtException', err => {
    console.error('Uncaught exception:', err)
})

const fastify = Fastify({ logger: false })

fastify.addHook('preHandler', (req, reply, done) => {
    reply.header('Content-Security-Policy', "default-src 'self' i.scdn.co; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline'")
    done()
})
fastify.register(fastifyStatic, {
    root: path.resolve('./public')
})
fastify.register(routes)

fastify.listen({ host: db.config.app.bind, port: db.config.app.port }).catch(e => {
    console.error(e)
    process.exit(1)
})

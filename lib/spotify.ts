import * as db from './db'
import * as url from 'url'
import * as http from 'http'
import * as https from 'https'
import open from 'open'
import { RecentlyPlayedResponse, UserProfile, HistoryEntryInsert } from './types'
import { IncomingMessage, ServerResponse } from 'http'

const redirect_uri = 'http://localhost:44482/__spotify_callback'
const scope = 'user-read-playback-state user-read-currently-playing user-read-recently-played'
export let nextRun = Date.now()

const refreshToken = (callback: (arg0?: Error) => void) => {
    const body = {
        refresh_token: db.config.spotify.refreshToken,
        grant_type: 'refresh_token'
    }
    const options = {
        headers: {
            'Authorization': 'Basic ' + (Buffer.from(db.config.spotify.clientId + ':' + db.config.spotify.clientSecret).toString('base64')),
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        hostname: 'accounts.spotify.com',
        path: `/api/token`,
    }
    const req = https.request(options, res => {
        if (res.statusCode === 200) {
            let out = ''
            res.on('data', d => out += d)
            res.on('end', () => {
                const j = JSON.parse(out)
                setToken(j.access_token, j.refresh_token, j.expires_in)
                callback()
            })
        } else {
            let out = ''
            res.on('data', d => out += d)
            res.on('end', () => callback(new Error(`Server responded with status ${res.statusCode}: ${out}`)))
        }
    })
    req.on('error', error => callback(error))
    const formBody = []
    for (let property in body) {
        const encodedKey = encodeURIComponent(property)
        const encodedValue = encodeURIComponent(String(body[<keyof typeof body> property]))
        formBody.push(`${encodedKey}=${encodedValue}`)
    }
    req.write(formBody.join('&'))
    req.end()
}
const queryJson = <T>(urlStr: string, callback: (arg0?: Error, arg1?: T) => void, lastTry = false) => {
    if (db.config.spotify.expires && (Date.now() > db.config.spotify.expires)) {
        refreshToken((err) => {
            err ? callback(err) : queryJson(urlStr, callback, true)
        })
        return // token is most likely expired, do not even attempt to use it
    }
    const q = url.parse(urlStr, true)
    const protocol = (q.protocol === 'http') ? http : https
    const options = {
        headers: {
            'Authorization': `Bearer ${db.config.spotify.accessToken}`,
            'Content-Type': 'application/json'
        },
        method: 'GET',
        hostname: q.hostname,
        port: q.port,
        path: `${q.pathname}${q.search || ''}`,
        protocol: q.protocol,
    }
    const req = protocol.request(options, res => {
        if (res.statusCode === 200) {
            let out = ''
            res.on('data', d => out += d)
            res.on('end', () => callback(undefined, JSON.parse(out)))
        }  else {
            let out = ''
            res.on('data', d => out += d)
            res.on('end', () => {
                if (!lastTry && res.statusCode === 401) {
                    let j
                    try { j = JSON.parse(out) } catch (e) { }
                    if (j && j.error && (j.error.message === 'The access token expired')) {
                        refreshToken((err) => {
                            err ? callback(err) : queryJson(urlStr, callback, true)
                        })
                    }
                }
                callback(new Error(`Server responded with status ${res.statusCode}: ${out}`))
            })
        }
    })
    req.on('error', error => {
        console.log(error)
        callback(error)
    })
    req.end()
}
const pullBestImage = (arr?: { url: string }[]) => {
    return (arr && arr.length) ? arr[0].url : ''
}

const queryProfile = () => new Promise<void>(resolve => {
    queryJson<UserProfile>('https://api.spotify.com/v1/me', (err, json) => {
        if (!err && json && json.id) {
            if (!db.config._appCache) db.config._appCache = {}
            db.config._appCache.profile = json
            db.saveConfig()
        }
        resolve()
    })
})
const querySpotifyData = () => new Promise<void>(resolve => {
    if (!db?.config?.spotify?.refresh) throw new Error('Missing config.spotify.refresh property!')
    nextRun = Date.now() + db.config.spotify.refresh
    console.log(`Updating data... (next update is scheduled for ${new Date(nextRun).toLocaleString()})`)
    const queryAfter = async (ts: number) : Promise<number> => new Promise((resolve, reject) => {
        queryJson<RecentlyPlayedResponse>(`https://api.spotify.com/v1/me/player/recently-played?after=${ts}&limit=50`, (err, json) => {
            if (err) {
                reject(err)
            } else {
                if (json && json.items.length) {
                    const items : HistoryEntryInsert[] = json.items.map(x => ({
                        id: x.track.id,
                        timestamp: new Date(x.played_at).getTime(),
                        title: x.track.name,
                        artist: x.track.artists.map(x => x.name).join(', '),
                        duration: x.track.duration_ms,
                        image: pullBestImage(x.track.album?.images)
                    }))
                    resolve(db.insertRecords(items).then(() => json.next ? json.cursors?.after : 0))
                } else {
                    resolve(0)
                }
            }
        })
    })
    db.lastTimestamp().then(async t => {
        let next = t + 1
        console.log((next === 1) ? 'Loading history from beginning of the year' : `Resuming at ${new Date(next).toLocaleString()}`)
        while (next !== 0) next = await queryAfter(next)
        return db.summarize().then(x => {
            console.log(`Tracking ${x[0]} records, counted ${Math.round(x[1] / 60000)} minutes`)
            resolve(queryProfile())
        })
    })
})

export const spotifyRange = (d: Date) : [number, number] => {
    const start = new Date(d.getFullYear(), 0, 1, 0, 0, 0, 0)
    const end = new Date(d.getFullYear(), 10, 1, 0, 0, 0, 0)
    if (d > end) {
        const nextYear = new Date(end)
        nextYear.setDate(nextYear.getDate() + 1)
        return spotifyRange(nextYear)
    } else return [start.getTime(), end.getTime()]
}
export const spotifySeason = (n: number) : [number, number] => {
    const currentSeason = spotifyRange(new Date())
    if (n === 0) return currentSeason
    const s = new Date(currentSeason[0])
    s.setFullYear(s.getFullYear() + n, 2)
    return spotifyRange(s)
}

export const onCreate = () => {
    const config = db.config
    if (config.spotify.accessToken) {
        querySpotifyData().then(() => {
            setInterval(querySpotifyData, config.spotify.refresh)
        })
    } else {
        console.log('Unable to load spotify data because access token is not set.')
        console.log('Please log into your spotify account and authorize this application to get accessToken automatically.')
        console.log('If this computer is running in headless mode, follow this link and set accessToken and refreshToken properties of config.spotify manually.')
        let openUrl = 'https://accounts.spotify.com/authorize'
        openUrl += '?response_type=code'
        openUrl += '&client_id=' + encodeURIComponent(db.config.spotify.clientId)
        openUrl += '&scope=' + encodeURIComponent(scope)
        openUrl += '&redirect_uri=' + encodeURIComponent(redirect_uri)
        console.log(openUrl)
        open(openUrl).catch(() => {})
        
        const requestListener = (req: IncomingMessage, res: ServerResponse) => {
            const queryObject = url.parse(req.url || '', true).query
            if (queryObject.error) {
                res.writeHead(200)
                res.end(`Error: ${queryObject.error}`)
            } else if (queryObject.code) {
                res.writeHead(200)
                res.end('OK')
                
                const body = {
                    code: queryObject.code,
                    redirect_uri: redirect_uri,
                    grant_type: 'authorization_code'
                }
                const options = {
                    headers: {
                        'Authorization': 'Basic ' + (Buffer.from(db.config.spotify.clientId + ':' + db.config.spotify.clientSecret).toString('base64')),
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    method: 'POST',
                    hostname: 'accounts.spotify.com',
                    path: `/api/token`,
                }
                const req = https.request(options, res => {
                    if (res.statusCode === 200) {
                        let out = ''
                        res.on('data', d => out += d)
                        res.on('end', () => {
                            const j = JSON.parse(out)
                            setToken(j.access_token, j.refresh_token, j.expires_in)
                            onCreate()
                        })
                    } else {
                        let out = ''
                        res.on('data', d => out += d)
                        res.on('end', () => {
                            console.error(`Failed to exchange code: Server responded with status ${res.statusCode}: ${out}`)
                        })
                    }
                })
                req.on('error', error => {
                    console.log(error)
                    console.error(`Failed to exchange code: Request failed: ${error.toString()}`)
                })
                const formBody = []
                for (let property in body) {
                    const encodedKey = encodeURIComponent(property)
                    const encodedValue = encodeURIComponent(String(body[<keyof typeof body> property]))
                    formBody.push(`${encodedKey}=${encodedValue}`)
                }
                req.write(formBody.join('&'))
                req.end()
            } else {
                res.writeHead(200)
                res.end('Invalid callback')
            }
        }
        const server = http.createServer(requestListener)
        server.listen(44482)
    }
}

export const setToken = (accessToken: string, refreshToken: string, expires = 0) => {
    db.config.spotify.accessToken = accessToken || db.config.spotify.accessToken
    db.config.spotify.refreshToken = refreshToken || db.config.spotify.refreshToken
    db.config.spotify.expires = expires ? new Date(Date.now() + (expires * 1000) - 10).getTime() : undefined
    db.saveConfig()
}

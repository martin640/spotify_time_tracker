import mysql from 'mysql'
import * as fs from 'fs'
import * as path from 'path'
import * as spotify from './spotify'
import {
    Config,
    DbHistoryCounterEntry,
    DbHistoryRangeEntry,
    DbSeasonStat, DbSimpleStat,
    HistoryEntryInsert,
    HistoryEntrySchema
} from './types'

const configPath = path.resolve('./config.json')
export let config : Config
if (fs.existsSync(configPath)) {
    config = JSON.parse(fs.readFileSync(configPath, 'utf-8'))
} else {
    console.error('Missing config.json')
    process.exit(1)
}
const con = mysql.createConnection(config.mysql)

export const saveConfig = () => fs.writeFileSync(configPath, JSON.stringify(config, null, 4), 'utf-8')

const connectDb = () => new Promise<void>(resolve => {
    con.connect(err => {
        if (err) {
            console.error(err)
            process.exit(1)
        } else resolve()
    })
})

const connectPromise = connectDb().then(spotify.onCreate)
export const waitForDatabase = () => connectPromise
export const nextUpdate = () => spotify.nextRun

const query = async <T>(queryStr: string, data: any[] = []) => new Promise<T[]>((resolve, reject) => {
    con.query(queryStr, data, (err, rows: T[]) => {
        err ? reject(err) : resolve(rows)
    })
})
const queryFirst = <T>(queryStr: string, data: any[] = [], fallback?: T) : Promise<T> => query<T>(queryStr, data).then(x => {
    if (x.length > 0) return x[0]
    else if (fallback) return fallback
    else throw new Error('Query returned no results')
})

export function insertRecords(items: HistoryEntryInsert[]) : Promise<void> {
    const inserts : any[][] = items.map(x => [
        x.id, x.timestamp, x.title, x.artist, x.duration, x.image
    ])
    return new Promise(resolve => {
        con.query('INSERT INTO `stt_history` (`id`, `timestamp`, `title`, `artist`, `duration`, `image`) VALUES ?', [inserts], (err) => {
            if (err && !err.toString().includes('ER_DUP_ENTRY')) console.warn(err)
            resolve()
        })
    })
}
export function lastTimestamp() : Promise<number> {
    return queryFirst('SELECT MAX(timestamp) as t FROM `stt_history`', [], { t: 0 }).then(x => x.t)
}
export function summarize() : Promise<[number, number]> {
    return queryFirst('SELECT COUNT(*) as c, SUM(duration) as s FROM `stt_history`', [], { c: 0, s: 0 }).then(x => [x.c, x.s])
}
export function querySeason(season: [number, number], add?: [number, number]) : Promise<DbSeasonStat> {
    return queryFirst<DbSeasonStat>('SELECT COUNT(*) as songs, SUM(duration) as duration FROM `stt_history` WHERE timestamp >= ? AND timestamp < ?',
        [season[0], season[1]], { songs: 0, duration: 0 }).then(x => ({ songs: x.songs + (add ? add[0] : 0), duration: x.duration + (add ? add[1] : 0) }))
}
export function queryN(season: [number, number], count: number) : Promise<DbHistoryRangeEntry[]> {
    return query<HistoryEntrySchema>('SELECT * FROM `stt_history` WHERE timestamp >= ? AND timestamp < ? ORDER BY timestamp DESC LIMIT ?', [season[0], season[1], count])
            .then(res => res.map(x => ({
                image: x.image,
                title: x.title,
                artist: x.artist,
                id: x.id,
                duration: x.duration,
                timestamp: x.timestamp
            })))
}
export function queryTop(season: [number, number], count: number) : Promise<DbHistoryCounterEntry[]> {
    return query<HistoryEntrySchema & { count: number }>('SELECT *, COUNT(id) as count FROM `stt_history` WHERE timestamp >= ? AND timestamp < ? GROUP BY id ORDER BY count DESC LIMIT ?',
        [season[0], season[1], count])
            .then(res => res.map(x => ({
                image: x.image,
                title: x.title,
                artist: x.artist,
                id: x.id,
                duration: x.duration,
                count: x.count
            })))
}
export function queryBy(season: [number, number], by: 'week'|'month') : Promise<DbSimpleStat<string>[]> {
    return query<DbSimpleStat<string>>(`SELECT ${by} as x, SUM(duration) as y FROM \`stt_history\` WHERE timestamp >= ? AND timestamp < ? GROUP BY ${by} ORDER BY timestamp`,
        [season[0], season[1]])
}

import { FastifyInstance } from 'fastify'
import { SingletonResponse } from './types'
import * as db from './db'
import { spotifySeason } from './spotify'

const getFiller = (season: [number, number]) : [number, number] => {
    const d = new Date(season[0])
    d.setMonth(2)
    if (!db.config.filler || (typeof db.config.filler[String(d.getFullYear())] === 'undefined')) return [0, 0]
    return db.config.filler[String(d.getFullYear())]
}

export default async (instance: FastifyInstance) => {
    await db.waitForDatabase()
    
    instance.get('/api/query', async () : Promise<SingletonResponse> => {
        const currentSeason = spotifySeason(0)
        const prevSeason = spotifySeason(-1)
        
        const ts = Date.now()
        const [last100, top10, season1, season2, lastTS, weekly, monthly] = await Promise.all([
            db.queryN(currentSeason, 100),
            db.queryTop(currentSeason, 10),
            db.querySeason(currentSeason, getFiller(currentSeason)),
            db.querySeason(prevSeason, getFiller(prevSeason)),
            db.lastTimestamp(),
            db.queryBy(currentSeason, 'week'),
            db.queryBy(currentSeason, 'month')
        ])
        console.log(`Querying database took ${Date.now() - ts} ms`)
        
        return {
            last100: last100,
            top10: top10,
            currentSeason: season1,
            previousSeason: season2,
            lastEntry: lastTS,
            nextUpdate: db.nextUpdate() - Date.now(),
            username: db.config._appCache?.profile?.display_name || '',
            weekly: weekly,
            monthly: monthly,
        }
    })
}
